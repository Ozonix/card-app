export default {
  creatingAccount: false,
  createAccountSuccessful: false,
  form: {
    firstName: '',
    middleInitial: '',
    lastName: '',
    dateOfBirth: '',
    ssn1: '',
    ssn2: '',
    ssn3: '',
    annualGrossIncome: '',
    addressLine1: '',
    addressLine2: '',
    zipCode: '',
    city: '',
    state: '',
    housingStatus: '',
    rentPayment1: '',
    rentPayment2: '',
    homePhone: '',
    email: '',
    employerName: '',
    workPhone: '',
    agreement: false
  },
  errors: {},
  currentStep: 0
}
