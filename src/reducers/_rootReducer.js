import {combineReducers} from 'redux';
import account from './reducerCardApp';

const rootReducer = combineReducers({
  account
});

export default rootReducer;
