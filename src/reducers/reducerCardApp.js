import initialState from './_initialState'
import { CREATING_ACCOUNT, CREATE_ACCOUNT_SUCCESSFUL, SET_FIELDS_FORM, SET_ERRORS_FORM, SET_STEP } from '../actions/_constants'

export default function (state = initialState, action) {
  switch (action.type) {
    case CREATING_ACCOUNT:
      console.log('CREATING_ACCOUNT Action')
      return creatingAccountReducer(state, action)
    case CREATE_ACCOUNT_SUCCESSFUL:
      console.log('CREATE_ACCOUNT_SUCCESSFUL Action')
      return createAccountSuccessfulReducer(state, action)
    case SET_FIELDS_FORM:
      console.log('SET_FIELDS_FORM Action')
      return setFieldsFormReducer(state, action)
    case SET_ERRORS_FORM:
      console.log('SET_ERRORS_FORM Action')
      return setErrorsFormReducer(state, action)
    case SET_STEP:
      console.log('SET_STEP Action')
      return setStep(state, action)
    default:
      return state;
  }
}

function creatingAccountReducer (state, action) {
  return Object.assign({}, state, { creatingAccount: action.creatingAccount })
}

function createAccountSuccessfulReducer (state, action) {
  return Object.assign({}, state, { createAccountSuccessful: action.createAccountSuccessful })
}

function setStep (state, action) {
  return Object.assign({}, state, { currentStep: action.currentStep })
}

function setFieldsFormReducer (state, action) {
  return Object.assign({}, state, { form: action.form })
}

function setErrorsFormReducer (state, action) {
  return Object.assign({}, state, { errors: action.errors })
}