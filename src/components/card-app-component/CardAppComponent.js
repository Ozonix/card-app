import React, { Component } from 'react'

import StepSlide_1 from './components/StepSlide_1'
import StepSlide_2 from './components/StepSlide_2'
import StepSlide_3 from './components/StepSlide_3'
import StepSlide_4 from './components/StepSlide_4'
import ReviewSlide from './components/ReviewSlide'
import Success from './components/Success'

class CardAppComponen extends Component {
  constructor(props) {
    super(props)

    this.validators = {
      firstName: /^([a-zA-ZáÁàÀâÂçÇéÉèÈêÊëËíÍîÎïÏóÓôÔúÚùÙûÛüÜñÑ](\. |[\-'\. ])?)+$/,
      middleInitial: /^[a-zA-ZáÁàÀâÂçÇéÉèÈêÊëËíÍîÎïÏóÓôÔúÚùÙûÛüÜñÑ]+$/,
      lastName: /^([a-zA-ZáÁàÀâÂçÇéÉèÈêÊëËíÍîÎïÏóÓôÔúÚùÙûÛüÜñÑ](\. |[\-'\. ])?)+$/,
      dateOfBirth: /^[0-1][0-9]\/[0-3][0-9]\/(19|20)[0-9]{2}$/,
      ssn1: /^[0-9]{3}\-?[0-9]{2}\-?[0-9]{4}$/,
      monthlyGrossIncome: /^\d+(\.\d{1,2})?$/,
      addressLine1: /^[A-Za-z\-\d\#\.\\\/',: ]+$/,
      addressLine2: /^[A-Za-z\-\d\#\.\\\/',: ]+$/,
      zipCode: /^[0-9]{5}$/,
      city: /^([A-Za-z]+[\-' ]*)+$/,
      props: /^[a-zA-Z]+$/,
      housingStatus: /^[a-zA-Z]+$/,
      rentPayment1: /^\d+(\.\d{1,2})?$/,
      rentPayment2: /^\d+(\.\d{1,2})?$/,
      homePhone: /^[0-9]{10}$/,
      email: /^\S*[0-9a-zA-Z]+\S*@\S*[0-9a-zA-Z]+\S*\.\S*[0-9a-zA-Z]+\S*$/,
      employerName: /^[A-Za-z\-\d\#\.\\\/',: ]+$/,
      workPhone: /^[0-9]{10}$/
    };
    
     this.requireds = [
      ['firstName','lastName','addressLine1','zipCode','city','props','homePhone','email'],
      ['ssn1','dateOfBirth'],
      ['employerName','rentPayment1'],
      ['housingStatus','rentPayment2'],
      ['agreement']
    ];
  }

  onBlur = (e) => {
    let errors = {};
    Object.assign(errors, this.props.errors);

    const pattern = this.validators[e.target.name];
    if(pattern && e.target.value !== '') {
      if(!RegExp(pattern).test(e.target.value)) {
        errors[e.target.name] = 'Please enter valid information';
        this.props.dispatchSetError(errors);
      } 
    }
  }

  onChange = (e) => {
 
    if(this.props.errors) {
      if(this.props.errors[e.target.name]) {
        this.props.dispatchSetError({});
      }
    }

    let form = {};
    Object.assign(form, this.props.form);
    form[e.target.name] = e.target.value;
    this.props.dispatchSetField(form);
  }

  onAgree = (e) => {
    let form = {};
    Object.assign(form, this.props.form);
    form['agreement'] = !form['agreement'];
    this.props.dispatchSetField(form);
  }

  toStep = (step) => {

    const currentStep = this.props.currentStep;
    let requireds = this.requireds[currentStep];

    let errors = {};
    Object.assign(errors, this.props.errors);

    let form = {};
    Object.assign(form, this.props.form);

    let canStep = true;

    if(currentStep != 4 && currentStep < step) {
      requireds.forEach(req => {
        if(form[req] == '') {
          errors[req] = `Please enter required field`;
          canStep = false;
        }
      });
    }

    for( let er in errors ) {
      if(er && currentStep < step) canStep = false;
    }

    if(canStep) {
      this.props.dispatchSetStep(step);
      this.props.dispatchSetError({});
    } else {
      this.props.dispatchSetError(errors);
    }
  }

  renderStep = (step) => {

    let rendering = [];

      if(step == 0) {
        rendering.push(<StepSlide_1 key="step_1" model={this.props.form} error={this.props.errors} change={this.onChange} blur={this.onBlur} step={this.toStep}></StepSlide_1>);
      }

      if(step == 1) {
        rendering.push(<StepSlide_2 key="step_2" model={this.props.form} error={this.props.errors} change={this.onChange} blur={this.onBlur} step={this.toStep}></StepSlide_2>);
      }

      if(step == 2) {
        rendering.push(<StepSlide_3 key="step_3" model={this.props.form} error={this.props.errors} change={this.onChange} blur={this.onBlur} step={this.toStep}></StepSlide_3>);
      }

      if(step == 3) {
        rendering.push(<StepSlide_4 key="step_4" model={this.props.form} error={this.props.errors} change={this.onChange} blur={this.onBlur} step={this.toStep}></StepSlide_4>);
      }

      if(step == 4) {
        rendering.push(<ReviewSlide key="review" model={this.props.form} error={this.props.errors} agree={this.onAgree} submit={this.handleSubmit} step={this.toStep}></ReviewSlide>);
      }

      if(step == 5 ) {
        rendering.push(<Success key="success"></Success>);
      }

      return rendering;
  }

  handleSubmit = () => {
    const { firstName, 
            middleInitial, 
            lastName, 
            addressLine1, 
            addressLine2, 
            zipCode, 
            city, 
            props, 
            homePhone, 
            workPhone, 
            email, 
            ssn1, 
            dateOfBirth, 
            employerName, 
            rentPayment1, 
            rentPayment2,
            monthlyGrossIncome, 
            housingStatus
    } = this.props.form;

    if(this.props.form.agreement) {
      /* delete the line below when backend is complete and working */
      this.props.dispatchSetStep(5);
      /* uncomment the line below when backend is completed and working */
      // return this.props.dispatchCreateAccountRequest(this.props.form)
      
    } else {
      let errors = {};
      Object.assign(errors, this.props.errors);
      errors['agreement'] = 'You must check this for continue';
      this.props.dispatchSetError(errors);
    }
  }

  render() {
    const step = this.props.currentStep;

    return (
        <div className="row td-body">
            <div className="col-lg-12">
                <h1>CREDIT CARD APPLICATION</h1>
                { step === 0 &&
                  <div id="important-info">
                      <p>Please note you must reside in the United States and be 18 years or older to apply. Mandatory fields are indicated with a *</p>
                      <p>IMPORTANT INFORMATION ABOUT PROCEDURES FOR OPENING A NEW ACCOUNT: To help the government fight the funding of terrorism and money laundering activities, federal law requires all financial institutions to obtain, verify, and record information that identifies each person who opens an account. What this means to you: When you open an account, we will ask for your name, street address, date of birth, social security number, and other information that will allow us to identify you. We may also ask to see copies of identifying documents.</p>
                  </div>
                }
                { step < 4 &&
                  <ul className="nav nav-tabs td-stages" role="tablist">
                      <li role="presentation" className="nav-item"><a id="step1link" href="#step1" className={ step >= 0 ? "nav-link active": "nav-link" } data-toggle="tab" aria-controls="step1" role="tab" title="Step 1" data-description="Personal Information">
                          { step > 0 ?
                            <i class="fa fa-check step-check" aria-hidden="true"></i> :
                            <span className="step-number">1</span> 
                          }
                          <span className="step-description">Personal Info</span>
                      </a></li>
                      <li role="presentation" className="nav-item"><a id="step2link" href="#step2" className={ step >= 1 ? "nav-link active": "nav-link" } data-toggle="tab" aria-controls="step2" role="tab" title="Step 2" data-description="Identify Information">
                          { step > 1 ?
                            <i class="fa fa-check step-check" aria-hidden="true"></i> :
                            <span className="step-number">2</span> 
                          }
                          <span className="step-description">Identity Info</span>
                      </a></li>
                      <li role="presentation" className="nav-item"><a id="step3link" href="#step3" className={ step >= 2 ? "nav-link active": "nav-link" } data-toggle="tab" aria-controls="step3" role="tab" title="Step 3" data-description="Employment Information">
                          { step > 2 ?
                            <i class="fa fa-check step-check" aria-hidden="true"></i> :
                            <span className="step-number">3</span> 
                          }
                          <span className="step-description">Employment Info</span>
                      </a></li>
                      <li role="presentation" className="nav-item"><a id="step4link" href="#step4" className={ step >= 3 ? "nav-link active": "nav-link" } data-toggle="tab" aria-controls="step4" role="tab" title="Step 4" data-description="Housing Info">
                          { step > 3 ?
                            <i class="fa fa-check step-check" aria-hidden="true"></i> :
                            <span className="step-number">4</span> 
                          }
                          <span className="step-description">Housing Info</span>
                      </a></li>
                  </ul>
                }
                <form role="form" id="needs-validation">
                    <div className="tab-content">
                        { this.renderStep(step) }
                        <div className="clearfix"></div>
                    </div>
                </form>
                <div className="col-lg d-none d-lg-block"></div>
            </div>
        </div>
    );
  }
}

export default CardAppComponen