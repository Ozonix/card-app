import React from 'react'
import Icon from 'react-fa'
import { Popover, OverlayTrigger, Button } from 'react-bootstrap'

const popoverClickRootClose = (
    <Popover id="popover-trigger-click-root-close" className="show popover-content">
        When you give us your mobile phone number, we have your
        permission to contact you at that number about all of your TD Retail
        Card Services accounts. Your consent allows us to use text messaging,
        artificial or pre-recorded voice messages and automatic dialling technology
        for informational and account service calls, but not for telemarketing
        or sales calls. It may include contact from companies working on our
        behalf to service your accounts. Message and data rates may apply. You may
        contact us anytime to change these preferences.
    </Popover>
);

const StepSlide_3 = ({model, error, change, blur, step}) => (
    <div className="tab-pane active" role="tabpanel" id="step3">
        <h2>Step 3: <i className="fa fa-lock"></i> Employment Information</h2>
        <div className="container-box">
            <div className="row">
                <div className="col-md-4 col-lg-6">
                    <div className="form-group required">
                        <label for="employer-name">Employer Name (or NA)</label>
                        <input  type="text" className="form-control" id="employer-name" placeholder="NA" required 
                                name="employerName"
                                value={ model.employerName }
                                onChange={ change }
                                onBlur={ blur }
                        />
                        { error.employerName && <div className="invalid-feedback" >{ error.employerName }</div> }
                    </div>
                </div>
                <div className="col-md-4 col-lg-4">
                    <div className="form-group">
                        <label for="secondary-phone">Secondary Phone Number</label> 
                        <OverlayTrigger
                            trigger="focus"
                            placement="top"
                            overlay={ popoverClickRootClose }>
                            <a tabIndex="0" className="popover-icon" role="button"> 
                                <i className="fa fa-info-circle" aria-hidden="true"></i>
                            </a>
                        </OverlayTrigger>   
                        <input  type="number" className="form-control" id="secondary-phone" placeholder="(999) 999 - 9999" 
                                name="workPhone"
                                value={ model.workPhone }
                                onChange={ change }
                                onBlur={ blur }
                        />
                        { error.workPhone && <div className="invalid-feedback" >{ error.workPhone }</div> }
                    </div>
                </div>
                <div className="col-md-4 col-lg-2">
                    <div className="form-group">
                        <label for="secondary-phone-type">Phone Type</label>
                        <select className="form-control" id="secondary-phone-type"
                            name="phoneType2"
                            value={ model.phoneType2 }
                            onChange={ change }
                            onBlur={ blur }
                        >
                            <option value="mobile">Mobile</option>
                            <option value="work">Work</option>
                        </select>
                    </div>
                </div>

            </div>
            <div className="row">
                <div className="col-md-4">
                    <div className="form-group required">
                        <label for="annual-income">Annual Gross Income</label>
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">$</span>
                            </div>
                            <input  type="text" id="annual-income" className="form-control" aria-label="Amount (to the nearest dollar)" placeholder="10,000" required 
                                    name="rentPayment1"
                                    value={ model.rentPayment1 }
                                    onChange={ change }
                                    onBlur={ blur }
                            />
                        </div>
                        { error.rentPayment1 && <div className="invalid-feedback" >{ error.rentPayment1 }</div> }
                    </div>
                </div>
            </div>
        </div>
        <p>
            <button type="button" className="btn btn-primary next-step btn-block-sm-only" onClick={() => { step(3)}}>Next Step</button>
            <button type="button" className="btn btn-outline-primary prev-step btn-block-sm-only" onClick={() => {step(1)}}>Go Back</button>
        </p>
        <p className="text-center-sm-only"><a href="#" data-toggle="modal" data-target="#confirmCancelModal">Cancel my Credit Card Application</a></p>
    </div>
)

export default StepSlide_3