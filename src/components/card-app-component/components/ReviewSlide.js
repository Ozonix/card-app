import React from 'react'
import Icon from 'react-fa'

const ReviewSlide = ({model, error, agree, submit, step}) => {

    return (
        <div>
            <div className="container-box" id="review">
                <div className="static-header">Step 1. Personal Info</div>
                <div className="row">
                    <div className="col-md-5">
                        <div className="static-group"><span className="label">First Name</span> <span className="field" id="static-first-name">{ model.firstName || '--' }</span></div>
                    </div>
                    <div className="col-md-2">
                        <div className="static-group"><span className="label">Middle Initial</span> <span className="field" id="static-middle-name">{ model.middleInitial || '--' }</span></div>
                    </div>
                    <div className="col-md-5">
                        <div className="static-group"><span className="label">Last Name</span> <span className="field" id="static-last-name">{ model.lastName || '--' }</span></div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-7">
                        <div className="static-group"><span className="label">Residential Home Address</span> <span className="field" id="static-address-line1">{ model.addressLine1 || '--' }</span>
                        </div>
                    </div>
                    <div className="col-md-5">
                        <div className="static-group"><span className="label">Address Line 2</span> <span className="field" id="static-address-line2">{ model.addressLine2 || '--' }</span></div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-2">
                        <div className="static-group"><span className="label">Zip Code</span> <span className="field" id="static-zip-code">{ model.zipCode || '--' }</span></div>
                    </div>
                    <div className="col-md-5">
                        <div className="static-group"><span className="label">City</span> <span className="field" id="static-city">{ model.city || '--' }</span></div>
                    </div>
                    <div className="col-md-5">
                        <div className="static-group"><span className="label">State</span> <span className="field" id="static-state">{ model.state || '--' }</span></div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-5">
                        <div className="static-group"><span className="label">Primary Phone Number</span> <span className="field" id="static-primary-phone">{ model.homePhone || '--' }</span></div>
                    </div>
                    <div className="col-md-2">
                        <div className="static-group"><span className="label">Phone Type</span> <span className="field" id="static-primary-phone-type">{ model.phoneType || '--' }</span></div>
                    </div>
                    <div className="col-md-5">
                        <div className="static-group"><span className="label">Email Address</span> <span className="field" id="static-email">{ model.email || '--' }</span></div>
                    </div>
                </div>
                <p className="d-none d-lg-block">
                    <button type="button" className="btn btn-outline-primary" data-toggle="modal" data-target="#step1Modal" onClick={() => {step(0)}}>Edit</button>
                </p>
            </div>

            <div className="container-box" id="review">
                <div className="static-header">Step 2. Identify Info</div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="static-group"><span className="label">Social Security Number</span> <span className="field" id="static-ssn">{ model.ssn1 || '--' }</span></div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="static-group"><span className="label">Date of Birth</span> <span className="field" id="static-address-line1">{ model.dateOfBirth || '--' }</span>
                        </div>
                    </div>
                </div>
                <p className="d-none d-lg-block">
                    <button type="button" className="btn btn-outline-primary" data-toggle="modal" data-target="#step1Modal" onClick={() => {step(1)}}>Edit</button>
                </p>
            </div>

            <div className="container-box" id="review">
                <div className="static-header">Step 3. Employment Info</div>
                <div className="row">
                    <div className="col-md-4">
                        <div className="static-group"><span className="label">Employment Information</span> <span className="field">{ model.employerName || '--' }</span></div>
                    </div>
                    <div className="col-md-4">
                        <div className="static-group"><span className="label">Secondary Phone Number</span> <span className="field" >{ model.workPhone || '--' }</span></div>
                    </div>
                    <div className="col-md-4">
                        <div className="static-group"><span className="label">Phone Type</span> <span className="field">{ model.phoneType2 || '--' }</span></div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4">
                        <div className="static-group"><span className="label">Annual Gross Income</span> <span className="field">{ model.rentPayment1 || '--' }</span>
                        </div>
                    </div>
                </div>
                <p className="d-none d-lg-block">
                    <button type="button" className="btn btn-outline-primary" data-toggle="modal" data-target="#step1Modal" onClick={() => {step(2)}}>Edit</button>
                </p>
            </div>

            <div className="container-box" id="review">
                <div className="static-header">Step 4. Housing Info</div>
                <div className="row">
                    <div className="col-md-5">
                        <div className="static-group"><span className="label">Housing Status</span> <span className="field" id="static-first-name">{ model.housingStatus || '--' }</span></div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-5">
                        <div className="static-group"><span className="label">Mounthly Gross Income</span> <span className="field" id="static-address-line1">{ model.rentPayment2 || '--' }</span>
                        </div>
                    </div>
                </div>
                <p className="d-none d-lg-block">
                    <button type="button" className="btn btn-outline-primary" data-toggle="modal" data-target="#step1Modal" onClick={() => {step(3)}}>Edit</button>
                </p>
            </div>

            <div className="review">
                <div className="review-box">
                <h3>Terms and Conditions</h3>
                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. <br/><br/>
                Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. <br/> <br/>
                Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
                </div>

                <div className="review-checking">
                <h3>By checking the 'I agree' box and submitting this application:</h3>
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</li>
                </ul>
                </div>

                <div className="review-checking">
                    <input type="checkbox" className="checkbox" value={ model.agreement } onChange={ agree } /> I Agree
                </div>
                { error.agreement && <div className="invalid-feedback">{ error.agreement }</div> }
            </div>

            <p>
                <button type="button" className="btn btn-primary next-step btn-block-sm-only" onClick={ submit }>Submit Application</button>
            </p>
            <p className="text-center-sm-only"><a href="#" data-toggle="modal" data-target="#confirmCancelModal">Cancel my Credit Card Application</a></p>
        </div>
    );
}

export default ReviewSlide