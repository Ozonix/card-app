import React from 'react'
import Icon from 'react-fa'

const StepSlide_2 = ({model, error, change, blur, step}) => (
    <div className="tab-pane active" role="tabpanel" id="step2">
        <h2>Step 2: <i className="fa fa-lock"></i> Identify Information</h2>
        <div className="container-box">
            <div className="row">
                <div className="col-md-6">
                    <div className="form-group required">
                        <label for="ssn">Social Security Number</label>
                        <input  type="text" className="form-control" id="ssn" placeholder="### - ## - ####" 
                                name="ssn1"
                                value={ model.ssn1 }
                                onChange={ change }
                                onBlur={ blur }
                        />
                        { error.ssn1 && <div className="invalid-feedback" >{ error.ssn1 }</div> }
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-6">
                    <div className="form-group required">
                        <label for="dob">Date of Birth</label>
                        <input  type="text" className="form-control" id="dob" placeholder="MM / DD / YYYY" 
                                name="dateOfBirth"
                                value={ model.dateOfBirth }
                                onChange={ change }
                                onBlur={ blur }
                        />
                        { error.dateOfBirth && <div className="invalid-feedback" >{ error.dateOfBirth }</div> }
                    </div>
                </div>
            </div>
        </div>
        <p>
            <button type="button" className="btn btn-primary next-step btn-block-sm-only" onClick={() => {step(2)}}>Next Step</button>
            <button type="button" className="btn btn-outline-primary prev-step btn-block-sm-only" onClick={() => {step(0)}}>Go Back</button>
        </p>
        <p className="text-center-sm-only"><a href="#" data-toggle="modal" data-target="#confirmCancelModal">Cancel my Credit Card Application</a></p>

    </div>
);

export default StepSlide_2