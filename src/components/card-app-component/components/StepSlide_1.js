import React from 'react'
import Icon from 'react-fa'
import { Popover, OverlayTrigger, Button } from 'react-bootstrap'

const popoverClickRootClose = (
    <Popover id="popover-trigger-click-root-close" className="show popover-content">
        When you give us your mobile phone number, we have your
        permission to contact you at that number about all of your TD Retail
        Card Services accounts. Your consent allows us to use text messaging,
        artificial or pre-recorded voice messages and automatic dialling technology
        for informational and account service calls, but not for telemarketing
        or sales calls. It may include contact from companies working on our
        behalf to service your accounts. Message and data rates may apply. You may
        contact us anytime to change these preferences.
    </Popover>
);

const StepSlide_1 = ({model, error, change, blur, step}) => {
    return (
        <div className="tab-pane active" role="tabpanel" id="step1">
            <h2>Step 1: <i className="fa fa-lock"></i> Personal Information</h2>
            <div className="container-box">
                <div className="row">
                    <div className="col-md-4 col-lg-5">
                        <div className="form-group required">
                            <label for="first-name">First Name</label>
                            <input  type="text" className="form-control" id="first-name" placeholder="First Name" 
                                    name="firstName"
                                    value={ model.firstName }
                                    onChange={ change }
                                    onBlur={ blur }
                            />
                            { error.firstName && <div className="invalid-feedback" >{ error.firstName }</div> }
                        </div>
                    </div>
                    <div className="col-md-4 col-lg-2">
                        <div className="form-group">
                            <label for="middle-name">Middle Initial</label>
                            <input  type="text" className="form-control" id="middle-name" placeholder="MI" 
                                    name="middleInitial"
                                    value={ model.middleInitial }
                                    onChange={ change }
                                    onBlur={ blur }
                            />
                        </div>
                    </div>
                    <div className="col-md-4 col-lg-5">
                        <div className="form-group required">
                            <label for="last-name">Last Name</label>
                            <input  type="text" className="form-control" id="last-name" placeholder="Last Name" 
                                    name="lastName"
                                    value={ model.lastName }
                                    onChange={ change }
                                    onBlur={ blur }
                            />
                            { error.lastName && <div className="invalid-feedback" >{ error.lastName }</div> }
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-7">
                        <div className="form-group required">
                            <label for="address-line1">Residential Home Address</label>
                            <input  type="text" className="form-control" id="address-line1" placeholder="Address Line 1" 
                                    name="addressLine1"
                                    value={ model.addressLine1 }
                                    onChange={ change }
                                    onBlur={ blur }
                            />
                            { !error.addressLine1 && <small id="emailHelp" className="form-text text-muted">Physical Address, no P.O. Boxes</small> }
                            { error.addressLine1 && <div className="invalid-feedback" >{ error.addressLine1 }</div> }
                        </div>
                    </div>
                    <div className="col-md-5">
                        <div className="form-group">
                            <label for="address-line2">Address Line 2</label>
                            <input  type="text" className="form-control" id="address-line2" placeholder="Address Line 2" 
                                    name="addressLine2"
                                    value={ model.addressLine2 }
                                    onChange={ change }
                                    onBlur={ blur }
                            />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4 col-lg-2">
                        <div className="form-group required">
                            <label for="zip-code">Zip Code</label>
                            <input  type="text" className="form-control" id="zip-code" placeholder="12345" 
                                    name="zipCode"
                                    value={ model.zipCode }
                                    onChange={ change }
                                    onBlur={ blur }
                            />
                            { error.zipCode && <div className="invalid-feedback" >{ error.zipCode }</div> }
                        </div>
                    </div>
                    <div className="col-md-4 col-lg-5">
                        <div className="form-group required">
                            <label for="city">City</label>
                            <input  type="text" className="form-control" id="city" placeholder="City" 
                                    name="city"
                                    value={ model.city }
                                    onChange={ change }
                                    onBlur={ blur }
                            />
                            { error.city && <div className="invalid-feedback" >{ error.city }</div> }
                        </div>
                    </div>
                    <div className="col-md-4 col-lg-5">
                        <div className="form-group required">
                            <label for="state">State</label>
                            <select className="form-control" id="state"
                                    name="state"
                                    value={ model.state }
                                    onChange={ change }
                                    onBlur={ blur }
                            >
                                <option>Select State</option>
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="CA">California</option>
                                <option value="CO">Colorado</option>
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="DC">District Of Columbia</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="HI">Hawaii</option>
                                <option value="ID">Idaho</option>
                                <option value="IL">Illinois</option>
                                <option value="IN">Indiana</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NV">Nevada</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NM">New Mexico</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="ND">North Dakota</option>
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR">Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                            { error.state && <div className="invalid-feedback" >{ error.state }</div> }
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4 col-lg-5">
                        <div className="form-group required">
                            <label for="primary-phone">Primary Phone Number</label> 
                            <OverlayTrigger
                                trigger="focus"
                                placement="top"
                                overlay={ popoverClickRootClose }>
                                <a tabIndex="0" className="popover-icon" role="button"> 
                                    <i className="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </OverlayTrigger>
                            <input  type="number" className="form-control" id="primary-phone" placeholder="(999) 999 - 9999" 
                                    name="homePhone"
                                    value={ model.homePhone }
                                    onChange={ change }
                                    onBlur={ blur }
                            />
                            { error.homePhone && <div className="invalid-feedback" >{ error.homePhone }</div> }
                        </div>
                    </div>
                    <div className="col-md-4 col-lg-2">
                        <div className="form-group required">
                            <label for="primary-phone-type">Phone Type</label>
                            <select className="form-control" id="primary-phone-type" 
                                    name="phoneType"
                                    value={ model.phoneType }
                                    onChange={ change }
                                    onBlur={ blur }
                            >
                                <option value="mobile">Mobile</option>
                                <option value="work">Work</option>
                            </select>
                            { error.phoneType && <div className="invalid-feedback" >{ error.phoneType }</div> }
                        </div>
                    </div>
                    <div className="col-md-4 col-lg-5">
                        <div className="form-group required">
                            <label for="email">Email Address</label>
                            <input  type="text" className="form-control" id="email" placeholder="abc@xyz.com" 
                                    name="email"
                                    value={ model.email }
                                    onChange={ change }
                                    onBlur={ blur }
                            />
                            { error.email && <div className="invalid-feedback" >{ error.email }</div> }
                        </div>
                    </div>
                </div>
            </div>
            <p>
                <button type="button" className="btn btn-primary next-step btn-block-sm-only" onClick={() => {step(1)}}>Next Step</button>
            </p>
            <p className="text-center-sm-only"><a href="#" data-toggle="modal" data-target="#confirmCancelModal">Cancel my Credit Card Application</a></p>
        </div>
    );
}

export default StepSlide_1