import React from 'react'

const StepSlide_1 = ({model, error, change, blur, step}) => (
    <div className="tab-pane active" role="tabpanel" id="step4">
        <h2>Step 4: <i className="fa fa-lock"></i> Housing Information</h2>
        <div className="container-box">
            <div className="row">
                <div className="col-md-4 col-lg-5">
                    <div className="form-group required">
                        <label for="housing-status">Housing Status</label>
                        <select className="form-control" id="housing-status"
                            name="housingStatus"
                            value={ model.housingStatus }
                            onChange={ change }
                            onBlur={ blur }
                        >
                            <option value="">Select</option>
                            <option value="rent">Rent</option>
                        </select>
                    </div>
                    { error.housingStatus && <div className="invalid-feedback" >{ error.housingStatus }</div> }
                </div>

            </div>
            <div className="row">
                <div className="col-md-4 col-lg-5">
                    <div className="form-group required">
                        <label for="annual-income">Mounthly Gross Income</label>
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">$</span>
                            </div>
                            <input  type="text" id="annual-income" className="form-control" aria-label="Amount (to the nearest dollar)" placeholder="2,000" required 
                                    name="rentPayment2"
                                    value={ model.rentPayment2 }
                                    onChange={ change }
                                    onBlur={ blur }
                            />
                        </div>
                        { error.rentPayment2 && <div className="invalid-feedback" >{ error.rentPayment2 }</div> }
                    </div>
                </div>
            </div>
        </div>
        <p>
            <button type="button" className="btn btn-primary next-step btn-block-sm-only" onClick={() => {step(4)}}>Review</button>
            <button type="button" className="btn btn-outline-primary prev-step btn-block-sm-only" onClick={() => {step(2)}}>Go Back</button>

        </p>
        <p className="text-center-sm-only"><a href="#" data-toggle="modal" data-target="#confirmCancelModal">Cancel my Credit Card Application</a></p>
    </div>
)

export default StepSlide_1