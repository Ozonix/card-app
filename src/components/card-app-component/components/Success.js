import React from 'react'

const Success = () => (
  <div>
    <div className="container-box" id="review">
        <div className="row">
          <div className="col-md-12">
              <div className="static-group"><span className="label">Congratulations</span> <span className="field">You've been approved for a XXXX card with a credit line of $2000</span></div>
          </div>
      </div>
    </div>
    <p className="text-center-sm-only"><a href="#" data-toggle="modal" data-target="#confirmCancelModal">Start new Credit Card Application</a></p>
  </div>
)

export default Success
