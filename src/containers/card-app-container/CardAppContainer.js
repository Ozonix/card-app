import React, {Component} from 'react'
import { connect } from 'react-redux'
import {func, bool, object, number} from 'prop-types'
import * as actions from '../../actions/actionsCardApp';

import CardAppComponent from '../../components/card-app-component/CardAppComponent'

CardAppComponent.propTypes = {
    dispatchCreateAccountRequest: func.isRequired,
    createAccountSuccessful: bool,
    form: object,
    errors: object,
    currentStep: number
}

const mapStateToProps = state => {
    return {
      createAccountSuccessful: state.account.createAccountSuccessful,
      creatingAccount: state.account.creatingAccount,
      form: state.account.form,
      errors: state.account.errors,
      currentStep: state.account.currentStep
    }
}
  
const mapDispatchToProps = dispatch => {
    return {
        dispatchCreateAccountRequest(userData) {
            dispatch(actions.addAccountRequest(userData))
        },

        dispatchSetError(error) {
            dispatch(actions.setErrorsForm(error));
        },

        dispatchSetField(form) {
            dispatch(actions.setFieldsForm(form));
        },

        dispatchSetStep(step) {
            dispatch(actions.setStep(step));
        }
    }
}

const CardAppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CardAppComponent)

export default CardAppContainer