import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import configureStore from '../configureStore';

import CardAppContainer from '../containers/card-app-container/CardAppContainer'

const store = configureStore();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div className="container">
            <div className="row td-header">
              <img src="assets/samplebanner.png" alt="Sample Banner" />
            </div>
            <Switch>
              <Route exact path='/' component={ CardAppContainer } />
            </Switch>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App
