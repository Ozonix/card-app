import * as actions from './_constants'
import axios from 'axios'

export function addAccountRequest(userData) {
  return dispatch => {
    dispatch(creatingAccount(true))
    return axios
      .post('https://jsonplaceholder.typicode.com', userData)
      .then(res => {
        dispatch(creatingAccount(false))
        dispatch(createAccountSuccessful(true))
      })
      .catch(err => {
        console.error('redux: addAccountRequest failed', err)
        dispatch(creatingAccount(false))
        dispatch(createAccountSuccessful(false))
      })
  }
}

export function creatingAccount(bool) {
  return {type: actions.CREATING_ACCOUNT, creatingAccount: bool}
}

export function createAccountSuccessful(bool) {
  return {type: actions.CREATE_ACCOUNT_SUCCESSFUL, createAccountSuccessful: bool}
}

export function setFieldsForm(form) {
  return {type: actions.SET_FIELDS_FORM, form}
}

export function setErrorsForm(errors) {
  console.log(errors);
  return {type: actions.SET_ERRORS_FORM, errors}
}

export function setStep(currentStep) {
  return {type: actions.SET_STEP, currentStep}
}